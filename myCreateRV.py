# ＃＃＃＃＃＃＃＃＃＃＃＃＃＃＃＃
#
# ○ 説明変数を用意するよ～～ ○
# 20個の選択肢の初めて採用された時間
# 20個の選択肢全てを触った回数
# 20個の選択肢全てを見た秒数
# 合計取り組み時間
# 作図取り組み時間の割合
# 作図取り組み時間を切り替えた回数
# ブロックを触る間隔の平均
# しばらく操作が止まっていたあとに
#
#
# ＃＃＃＃＃＃＃＃＃＃＃＃＃＃＃＃

class Color:
    BLACK     = '\033[30m'
    RED       = '\033[31m'
    GREEN     = '\033[32m'
    YELLOW    = '\033[33m'
    BLUE      = '\033[34m'
    PURPLE    = '\033[35m'
    CYAN      = '\033[36m'
    WHITE     = '\033[37m'
    END       = '\033[0m'
    BOLD      = '\038[1m'
    UNDERLINE = '\033[4m'
    INVISIBLE = '\033[08m'
    REVERCE   = '\033[07m'


import pprint
import numpy as np
import pandas as pd


def myCreateRV(df_list, filename):
    df = pd.read_csv("survey.csv", header=0)
    df = df.set_index('名前');

    df = df.loc[filename]

    return df