def loadAnswers():
    path = 'Answers.txt'

    with open(path, encoding="UTF-8") as f:
        s = f.read()
        list = s.split("\n\n")
        # for i, val in enumerate(list):
        #     print(val)
        f.close()
    return list

def loadQuestion():
    path = 'Question.txt'

    with open(path, encoding="UTF-8") as f:
        s = f.read()
        list = s.split("\n\n")
        # for i, val in enumerate(list):
        #     print(val)
        f.close()
    return list