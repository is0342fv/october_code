import pandas as pd
import warnings
import numpy as np
from sklearn import linear_model
import psutil
# 交差検証
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_val_predict

# ロジスティック回帰
from sklearn.linear_model import LogisticRegression
# SVM
from sklearn.svm import LinearSVC
# 決定木
from sklearn.tree import  DecisionTreeClassifier
# k-NN
from sklearn.neighbors import  KNeighborsClassifier
# ランダムフォレスト
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.externals.six import StringIO
from sklearn.tree import export_graphviz
import pydotplus
import numpy as np
import matplotlib.pyplot as plt
import japanize_matplotlib
import matplotlib.cm as cm
import pprint

search_params = {
     'n_estimators'      : [6, 7, 8, 9, 10],
      'max_features'      : [2, 3, 4, 6, 7],
      'random_state'      : [1028],
      'n_jobs'            : [1],
      'min_samples_split' : [2, 3, 5, 6, 7],
      'max_depth'         : [2, 3, 4, None]
}
search_paramsLR = {
    'C': np.linspace(10, 9, 10, endpoint=False),
    'solver': ["liblinear"],
    'penalty': ["l2"],
    'random_state': [1028]
}

class Color:
    BLACK     = '\033[30m'
    RED       = '\033[31m'
    GREEN     = '\033[32m'
    YELLOW    = '\033[33m'
    BLUE      = '\033[34m'
    PURPLE    = '\033[35m'
    CYAN      = '\033[36m'
    WHITE     = '\033[37m'
    END       = '\033[0m'
    BOLD      = '\038[1m'
    UNDERLINE = '\033[4m'
    INVISIBLE = '\033[08m'
    REVERCE   = '\033[07m'

def kfold(exp, res, params1, params2):
    print(Color.GREEN + "＝＝＝＝＝＝＝＝＝交差検証結果＝＝＝＝＝＝＝＝＝" + Color.END)
    models = [LogisticRegression(**params1), RandomForestClassifier(**params2)]
    scores = {}
    cmat = {}
    accuracy = {}
    precision = {}
    recall = {}
    f1 = {}
    for model in models:
        #scores[str(model).split('(')[0]] = cross_val_score(model, exp, res, cv=KFold(n_splits=17, shuffle=True, random_state=0))
        res_pred = cross_val_predict(model, exp, res, cv=KFold(n_splits=17, shuffle=True, random_state=0))
        cmat[str(model).split('(')[0]] = confusion_matrix(res, res_pred)
        accuracy[str(model).split('(')[0]] = accuracy_score(res, res_pred)
        precision[str(model).split('(')[0]] = precision_score(res, res_pred)
        recall[str(model).split('(')[0]] = recall_score(res, res_pred)
        f1[str(model).split('(')[0]] = f1_score(res, res_pred)
    #df = pd.DataFrame(scores)

    #print(df.mean())
    for mykey, myvalue in cmat.items():
        print(Color.CYAN + "〇" + str(mykey) + "〇" + Color.END)
        print(str(myvalue))
        print("正解率" + str( np.round(accuracy[str(mykey)], 3) ), end=", ")
        print("精度" + str( np.round(precision[str(mykey)], 3) ), end=", ")
        print("再現率" + str( np.round(recall[str(mykey)], 3) ), end=", ")
        print("F値" + str( np.round(f1[str(mykey)], 3) ), end="\n")

def pred_percentage(exp, res, df):
    ranking_df = pd.DataFrame()
    proba_list = []
    name_list = []
    for name in enumerate(df.index):
        new_exp = exp.drop(str(name[1]), axis=0)
        new_res = res.drop(str(name[1]), axis=0)
        # print("------------------------")
        # print(Color.GREEN + "〇" + str(name[1]) + "さんの結果" + Color.END)
        coef, proba = logistic(new_exp, new_res, df, str(name[1]))
        name_list.append(str(name[1]))
        proba_list.append(proba[0][1])
    # print("------------------------")
    ranking_df = pd.DataFrame(proba_list, index=name_list)
    print(Color.CYAN + "★理解度ランキング★" + Color.END)
    print(ranking_df.sort_values(by=0, ascending=False))

def logistic(explanatory, responce, df, predict_name):
    random = 0
    clf = LogisticRegression(**my_options)
    clf.fit(explanatory, responce)

    regression_coefficient = clf.coef_
    segment = clf.intercept_
    #print("回帰係数：{}".format(regression_coefficient))
    # print("切片：{}".format(segment))
    # print("決定係数：{}".format(clf.score(explanatory, responce)))

    test = df.loc[predict_name]
    tlen = len(test.T)-3
    x_test = test.loc[0:tlen]
    x_test = pd.DataFrame(x_test).T

    predict = clf.predict(x_test)
    predict_proba = np.round(clf.predict_proba(x_test), 3)

    # print(Color.RED + "検証結果(２値):{}".format(predict) + Color.END)
    # print(Color.BLUE + "検証結果(確率):{}".format(predict_proba) + Color.END)

    return regression_coefficient, predict_proba

def myLogistic(df):
    warnings.simplefilter('ignore')

    explanatory = df.loc[:,range(0, len(df.T)-2)]
    #print(explanatory)
    raw_responce = df.loc[:,range(len(df.T)-2, len(df.T))]
    #print(raw_responce)

    #explanatory = explanatory.drop(predict_name, axis=0)
    #raw_responce = raw_responce.drop(predict_name, axis=0)


    print(Color.REVERCE + "＝＝＝＝＝＝＝＝＝＝＝＝＝知識力＝＝＝＝＝＝＝＝＝＝＝＝＝" + Color.END)
    responce = raw_responce.loc[:, len(df.T)-2]
    responce = pd.DataFrame(responce)
    all_know, each_know = myLRandRF(explanatory, responce, df)

    print(Color.REVERCE + "＝＝＝＝＝＝＝＝＝＝＝＝＝論理力＝＝＝＝＝＝＝＝＝＝＝＝＝" + Color.END)
    responce = raw_responce.loc[:, len(df.T)-1]
    all_logic, each_logic = myLRandRF(explanatory, responce, df)

    print("====ランキングまとめ====")
    all_know = all_know.reset_index().rename(columns={0: "学習項目到達度"})
    all_logic = all_logic.reset_index().rename(columns={0: "プログラミング的思考力"})
    each_know = each_know.reset_index().rename(columns={0: "学習項目到達度"})
    each_logic = each_logic.reset_index().rename(columns={0: "プログラミング的思考力"})
    map_all = pd.merge(all_know, all_logic)
    map_each = pd.merge(each_know, each_logic)

    # damesou
    list1 = draw_map(map_all, "image_all")
    list2 = draw_map(map_each, "image_each")
    print(list2.sort_values(by=0))

    return 0

def show_coef(data):
    for i, elem in enumerate(data):
        data[i] = (round(elem, 4))


    label = []
    for i in range(20):
        label.append("first" + str(i+1))
    for i in range(20):
        label.append("touch" + str(i+1))
    for i in range(20):
        label.append("hover" + str(i+1))
    for i in range(12):
        label.append("空欄" + str(i+1) + "類似度低下回数")
    for i in range(12):
        label.append("空欄" + str(i + 1) + "類似度")
    label.append("模範解答類似度")
    label.append("類似度低下回数")
    label.append("合計取組時間")
    label.append("作図取組割合")
    label.append("作図切替回数")
    label.append("タッチ頻度")
    label.append("タッチ頻度~1/4時間")
    label.append("タッチ頻度~2/4時間")
    label.append("タッチ頻度~3/4時間")
    label.append("タッチ頻度~4/4時間")
    label.append("タッチ頻度分散")
    label.append("タッチ回数分散")
    label.append("ホバー秒数分散")

    df = pd.DataFrame({'coef': pd.Series(data)})
    df['name'] = label
    df = df.sort_values('coef', ascending=False)
    df = df.reset_index(drop=True)
    df.index = df.index + 1
    pd.options.display.max_columns = None
    pd.options.display.width = None
    print(df.T)

    return df

    # print(first)
    # print(touched)
    # print(hovered)
    # print(all_time)
    # print(draw_rate)
    # print(draw_num)
    # print(touch_interval)

def graphviz(estimator, i):
    from sklearn import tree
    estimator = estimator
    filename = "./tree"+str(i)+".png"
    dot_data = tree.export_graphviz(
        estimator,
        # out_file=None,
        # filled=True,
        # rounded=True,
        # feature_names=features,
        # class_names=iris.target_names,
        # special_characters=True
    )
    graph = pydotplus.graph_from_dot_data(dot_data)
    graph.progs = {'dot': u"release/bin/dot.exe"}
    graph.write_png(filename)



def pred_percentage(exp, res, df):
    ranking_df = pd.DataFrame()
    proba_list = []
    name_list = []
    for name in enumerate(df.index):
        new_exp = exp.drop(str(name[1]), axis=0)
        new_res = res.drop(str(name[1]), axis=0)
        # print("------------------------")
        # coef, proba = logistic(new_exp, new_res, df, str(name[1]))
        gs = GridSearchCV(LogisticRegression(), search_paramsLR,
                          cv=KFold(n_splits=16, shuffle=True, random_state=None), verbose=False, n_jobs=-1)
        gs.fit(new_exp, new_res.values.ravel())
        clf = LogisticRegression(C=gs.best_params_["C"], solver=gs.best_params_["solver"], penalty=gs.best_params_["penalty"], random_state=gs.best_params_["random_state"])
        clf.fit(new_exp, new_res)

        test = df.loc[name[1]]
        tlen = len(test.T) - 3
        x_test = test.loc[0:tlen]
        x_test = pd.DataFrame(x_test).T
        proba = clf.predict_proba(x_test)
        # print(Color.GREEN + "〇" + str(name[1]) + "さんの結果" + Color.END)
        # print(exp.loc[str(name)])
        # print(proba)
        name_list.append(str(name[1]))
        proba_list.append(proba[0][1])
    # print("------------------------")
    ranking_df = pd.DataFrame(proba_list, index=name_list)
    print(Color.CYAN + "★理解度ランキング★" + Color.END)
    print(ranking_df.sort_values(by=0, ascending=False))

    return ranking_df


def myLRandRF(explanatory, responce, df):
    #### ロジスティック回帰ィイイイイイイイイイイイ！！ ####
    # 全てを学習データにしてモデルを再構築
    gs = GridSearchCV(LogisticRegression(), search_paramsLR,
                      cv=KFold(n_splits=17, shuffle=True, random_state=None), verbose=True, n_jobs=-1)
    gs.fit(explanatory, responce.values.ravel())
    clf = LogisticRegression(C=gs.best_params_["C"], solver=gs.best_params_["solver"],
                             penalty=gs.best_params_["penalty"], random_state=gs.best_params_["random_state"])
    clf.fit(explanatory, responce)
    knowledge = clf.coef_
    logi_param = gs.best_params_

    ranking_df = pd.DataFrame()
    name_list = []
    proba_list = []
    for name in enumerate(df.index):
        test = df.loc[name[1]]
        tlen = len(test.T) - 3
        x_test = test.loc[0:tlen]
        x_test = pd.DataFrame(x_test).T
        proba = clf.predict_proba(x_test)
        # print(Color.GREEN + "〇" + str(name[1]) + "さんの結果" + Color.END)
        # print(exp.loc[str(name)])
        # print(proba)
        name_list.append(str(name[1]))
        proba_list.append(proba[0][1])
    # print("------------------------")
    all_ranking_df = pd.DataFrame(proba_list, index=name_list)

    #### ランダムフォレストォオオオオオオオ ####
    # clf2 = RandomForestClassifier(**my_options2)
    # clf2.fit(explanatory, responce)
    #### グリッドサーチしてみる
    p = psutil.Process()
    p.nice(psutil.HIGH_PRIORITY_CLASS)
    print("PID: %s  優先度： %s" % (p.pid, p.nice()))
    gs = GridSearchCV(RandomForestClassifier(), search_params, cv=KFold(n_splits=17, shuffle=True, random_state=None), verbose=True, n_jobs=-1)
    gs.fit(explanatory, responce.values.ravel())
    RF_param = gs.best_params_
    # gs.best_estimator_.fit(explanatory, responce)
    ####

    # 9. 可視化　(PDFで保存）
    dot_data = StringIO()
    best_max_depth = gs.best_params_["max_depth"]
    best_max_features = gs.best_params_["max_features"]
    best_min_samples_split = gs.best_params_["min_samples_split"]
    best_n_estimators = gs.best_params_["n_estimators"]
    best_n_jobs = gs.best_params_["n_jobs"]
    best_random_state = gs.best_params_["random_state"]
    clf2 = RandomForestClassifier(max_depth=best_max_depth, max_features=best_max_features, min_samples_split=best_min_samples_split, n_estimators=best_n_estimators, n_jobs=best_n_jobs, random_state=best_random_state)
    clf2.fit(explanatory, responce)
    for i, val in enumerate(clf2.estimators_):
        graphviz(val, i)

    each_ranking_df = pred_percentage(explanatory, responce, df)
    kfold(explanatory, responce, logi_param, RF_param)

    # 全てを学習データにしたモデルの回帰係数一覧を表示
    print(Color.YELLOW + "〇〇〇〇　回帰係数 by ランダムフォレスト　〇〇〇〇" + Color.END)
    show_coef(gs.best_estimator_.feature_importances_).to_csv("knowledge_coef_random_l2.csv", encoding="SHIFT-JIS")
    print(Color.YELLOW + "〇〇〇〇　回帰係数 by ロジスティック回帰　〇〇〇〇" + Color.END)
    show_coef(knowledge[0]).to_csv("knowledge_coef_logistic_l2.csv", encoding="SHIFT-JIS")

    # print(all_ranking_df, each_ranking_df)
    return all_ranking_df, each_ranking_df

def draw_map(df_map, filename):
    name_list = []
    schema_list = []
    plt.figure(figsize=(10, 10), dpi=100)
    x = np.linspace(0, 1, 10)
    y = x
    plt.plot(x, y, color="blue", linestyle="dashed", alpha=0.3)
    for index, row in df_map.iterrows():
        label = row["index"]
        x = row["学習項目到達度"]
        y = row["プログラミング的思考力"]
        z = np.dot(np.array([x, y]), np.array([1/np.sqrt(2), 1/np.sqrt(2)]))
        # gosa = (x - y)**2
        # if gosa==0:
        #     gosa = 0.00000000001
        # # z = z - gosa
        plt.scatter(x, y, s=15, cmap="inferno")
        # plt.annotate(label + ", "+str(np.round(z-gosa, 2)), xy=(x, y))
        plt.annotate(label + ", "+str(np.round(z, 2)), xy=(x, y))
        name_list.append(label)
        # schema_list.append(np.round(z-gosa, 3))
        schema_list.append(np.round(z, 3))

        # plt.show()
    plt.savefig(filename + ".png")

    return pd.DataFrame(schema_list, index = name_list)