from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import matplotlib.cm as cm
import numpy as np

sns.set_style("darkgrid")

label = [
    'while(input \空欄)',
    'run(' ', \空欄);',
    'int main()',
    'while(i \空欄)',
    'int input;',
    'int i;',
    'int input[][];',
    'printf("正の整数: ");',
    'run("*", \空欄);',
    'printf("\空欄", input);',
    'int input[];',
    'int i;',
    'for(i = \空欄; \空欄; i++)',
    'printf("\空欄", c);',
    '\空欄 run(\空欄 c, \空欄 length)',
    'scanf("\空欄", \空欄);',
    'for(input = \空欄; \空欄; input++)',
    'for(i = \空欄; \空欄; i++)',
    'printf("\空欄");',
    'printf("\空欄: ", i);'
]


def get_Z(df, x, y):
    return df.iat[y, x]

def detectBlock(column_name):
    for i in range(len(label)):
        replaced = column_name.replace("block_touch_num_", "")
        if(i == int(replaced)):
            return label[i]
    return None

def myPlot(df_list, filename):
    df_user = df_list[0]
    df_time_stamp = df_list[1]
    df_code = df_list[2]
    df_time = df_list[3]
    df_touch = df_list[4]
    df_hover = df_list[5]

#    df_touch = df_touch.sort_values(list(reversed(df_touch.index)), axis=1, ascending=False)

    fig = plt.figure(figsize=(15, 10))
    ax = fig.add_subplot(111, projection='3d')

    cutsize = int(np.shape(df_time_stamp)[0]/10) #時間軸を何分割するか
    cutsize = int(np.shape(df_time_stamp)[0]/cutsize)
    for i in range(0, np.shape(df_touch)[1]):
        for j in range(0, np.shape(df_time_stamp)[0], cutsize):
            if(get_Z(df_touch, i, j) >= 0):
                colorcode = cm.hsv(i/np.shape(df_touch)[1])
                search = detectBlock(df_touch.columns[i]).split("\空欄")
                flag = 0
                for element in search:
                    if(pd.isnull(df_code[j])):
                        break
                    if(element in df_code[j]):
                        flag = 1
                    else:
                        flag = 0
                if(flag!=1):
                    colorcode = (colorcode[0]/5, colorcode[1]/5, colorcode[2]/5, 0.3)

                ax.bar3d(i, j, 0, 0.1, cutsize*0.6, get_Z(df_touch, i, j), color = colorcode, linewidth=0, shade=True)

    ax.set_ylabel('time')
    ax.set_zlabel('touch count')
    ax.set_zlim(0, 10)
    ax.set_ylim(0, 320)

    # ブロック番号を実際のコードに変換
    for element in df_touch:
        for i in range(np.shape(df_touch)[1]):
            rep_element = element.replace("block_touch_num_", "")
            if(i == int(rep_element)):
                df_touch.rename(columns={element:label[i]}, inplace=True)

    plt.xticks(np.arange((np.shape(df_touch)[1])), df_touch.columns)

    plt.setp(ax.get_xticklabels(), rotation=30, horizontalalignment='right')

    plt.savefig("touch_" + filename + ".png")
    #plt.show()

################################

#    df_hover = df_hover.sort_values(list(reversed(df_hover.index)), axis=1, ascending=False)

    fig = plt.figure(figsize=(15, 10))
    ax = fig.add_subplot(111, projection='3d')

    cutsize = int(np.shape(df_time_stamp)[0]/10) #時間軸を何分割するか
    cutsize = int(np.shape(df_time_stamp)[0]/cutsize)
    for i in range(0, np.shape(df_hover)[1]):
        for j in range(0, np.shape(df_time_stamp)[0], cutsize):
            if(get_Z(df_hover, i, j) > 0):
                ax.bar3d(i, j, 0, 0.1, cutsize*0.6, get_Z(df_hover, i, j), color = cm.hsv(i/np.shape(df_hover)[1]), linewidth=0, shade=True)

    ax.set_ylabel('time')
    ax.set_zlabel('hover count')
    ax.set_zlim(0, 100000)
    ax.set_ylim(0, 320)

    # ブロック番号を実際のコードに変換
    for element in df_hover:
        for i in range(np.shape(df_hover)[1]):
            rep_element = element.replace("block_hover_time_", "")
            if(i == int(rep_element)):
                df_hover.rename(columns={element:label[i]}, inplace=True)

    plt.xticks(np.arange((np.shape(df_hover)[1])), df_hover.columns)

    plt.setp(ax.get_xticklabels(), rotation=30, horizontalalignment='right')

    plt.savefig("hover_" + filename + ".png")
    #plt.show()

################################


    fig = plt.figure(figsize=(15, 10))
    ax = fig.add_subplot(111, projection='3d')

    cutsize = 70 #時間軸を何分割するか
    cutsize = int(np.shape(df_time_stamp)[0]/cutsize)
    for i in range(0, np.shape(df_time)[1]):
        for j in range(0, np.shape(df_time_stamp)[0], cutsize):
            if(get_Z(df_time, i, j) > 0):
                ax.bar3d(i, j, 0, 1, cutsize*0.6, get_Z(df_time, i, j), color = cm.hsv(i/np.shape(df_time)[1]), linewidth=0, shade=True)

    ax.set_ylabel('time')
    ax.set_zlabel('work')
    ax.set_zlim(0, 300000)
    ax.set_ylim(0, 320)

    plt.xticks(np.arange((np.shape(df_time)[1])), df_time.columns)

    plt.setp(ax.get_xticklabels(), rotation=30, horizontalalignment='right')

    plt.savefig("time_" + filename + ".png")
    #plt.show()

