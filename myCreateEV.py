# ＃＃＃＃＃＃＃＃＃＃＃＃＃＃＃＃
#
# ○ 説明変数を用意するよ～～ ○
# 20個の選択肢の初めて採用された時間
# 20個の選択肢全てを触った回数
# 20個の選択肢全てを見た秒数
# 合計取り組み時間
# 作図取り組み時間の割合
# 作図取り組み時間を切り替えた回数
# ブロックを触る間隔の平均
# しばらく操作が止まっていたあとに
#
#
# ＃＃＃＃＃＃＃＃＃＃＃＃＃＃＃＃

class Color:
    BLACK     = '\033[30m'
    RED       = '\033[31m'
    GREEN     = '\033[32m'
    YELLOW    = '\033[33m'
    BLUE      = '\033[34m'
    PURPLE    = '\033[35m'
    CYAN      = '\033[36m'
    WHITE     = '\033[37m'
    END       = '\033[0m'
    BOLD      = '\038[1m'
    UNDERLINE = '\033[4m'
    INVISIBLE = '\033[08m'
    REVERCE   = '\033[07m'


import pprint
import numpy as np
import pandas as pd
import sklearn.preprocessing as preprocessing
import re
import Levenshtein
import myLoad
import myScoreBlank
from scipy.signal import argrelmin
from scipy.signal import argrelmax
import matplotlib.pyplot as plt
import copy

# pd.set_option('display.max_columns', 500)
# pd.set_option('display.max_rows', 500)

def score_sim(df_code, answer, filename):
    sim = []
    bad_words = ["int input;", "int i;"]
    for i, val in enumerate(df_code):
        # print(filename + "さん : " + str(i) + "番目")
        if val == val:
            # bad_words を削除
            val_line = val.split("\n")
            delete = []
            for line_index, line in enumerate(val_line):
                for bad_word in bad_words:
                    if (bad_word in line):
                        delete.append(line_index)
                        # sep_question.pop(i)
            delete.reverse()
            for line_index, val2 in enumerate(delete):
                val_line.pop(val2)
            # print(sep_an_answer)
            # val_lineを一つの文字列に
            s = ""
            for index, line in enumerate(val_line):
                s += line
            s = s.replace("row", "input")
            s = s.replace('"\r\n"', '"\\n"').replace("\r", "\n").replace('"\n"', '"\\n"')
            s = s.replace("%d\n", "%d")
            s = s.replace(' ', '')
            # 正解と比較
            ratio = []
            for index, val in enumerate(answer):
                # bad_words を削除
                val_line = val.split("\n")
                delete = []
                for line_index, line in enumerate(val_line):
                    for bad_word in bad_words:
                        if (bad_word in line):
                            delete.append(line_index)
                            # sep_question.pop(i)
                delete.reverse()
                for line_index, val2 in enumerate(delete):
                    val_line.pop(val2)
                # val_lineを一つの文字列に
                answer_ = ""
                for index, a in enumerate(val_line):
                    answer_ += a + "\n"
                answer_ = answer_.replace('"\r\n"', '"\\n"').replace("\r", "\n")
                answer_ = answer_.replace(' ', '')
                # print(answer_)
                ratio.append(Levenshtein.ratio(s, answer_))
            sim.append(max(ratio))
            # print("類似度は，" + str(max(ratio)) + "です♪")
            # その人のi*5秒目のコード
            # print(s)
        else:
            sim.append(0)


    #
    # print("正解は～～～CMの後！")
    # print("---ここでCM----")
    # for i, val in enumerate(answer):
    #     print("★" + str(i+1) + "番目の解答例")
    #     print(val)

    return sim

def myCreateEV(df_list, filename):
    print("＝＝＝＝＝＝＝＝\t" + filename + "\t＝＝＝＝＝＝＝＝")
    first = []
    touched = None
    hovered = None
    all_time = None
    draw_rate = None
    draw_num = None
    touch_interval = None #sec

    touched = np.array(df_list['touch'].tail(1).values.tolist()).reshape(-1).tolist()
    hovered = np.array(df_list['hover'].tail(1).values.tolist()).reshape(-1).tolist()
    hovered = list(map(lambda x: round(x/1000, 2), hovered))

    # mx = max(touched)
    # for i, elm in enumerate(touched):
    #     touched[i] = touched[i] / mx
    # mx = max(hovered)
    # for i, elm in enumerate(hovered):
    #     hovered[i] = hovered[i] / mx

    t_list = df_list['touch'].T.values.tolist()
    for i, touch_list in enumerate(t_list, 0):
        for j, elem in enumerate(touch_list):
            if(elem != 0):
                first.append(j)
                break
            elif(j >= len(touch_list)-1):
                first.append(j)
    # 値が大きければ大きいほど採用時間が早い
    for i, elem in enumerate(first, 0):
        first[i] = max(first) - first[i]
        first[i] = first[i] / max(first)

    #### 空欄があっているのか！？ ####
    answer = []
    answer = myLoad.loadAnswers()
    answer2 = copy.copy(answer)
    question = myLoad.loadQuestion()
    # print(question[0])
    blank_df = myScoreBlank.scoreBlank(question[0], df_list["code"], answer2, filename)
    # print(blank_df)
    max_blank = blank_df.max(axis=1).tolist()
    # print(max_blank)
    max_blank.pop(-1)
    max_blank.pop(-1)
    # print(max_blank)
    missed_know = []
    for j, a_column in blank_df.iterrows():
        mx = 0
        downed = 0
        count = 0
        for i, val in enumerate(a_column):
            if mx-0.005 > val:
                downed += 1
            else:
                mx = val
                downed = 0
            if downed > 1:
                mx = val
                count += 1
                downed = 0
        missed_know.append(count)
    missed_know.pop(-1)
    missed_know.pop(-1)
    # print(missed_know)

    #########ゴリッゴリゴリゴリゴリゾーーーーン
    # print(max_blank)
    max_blank[0] = max(max_blank[0:4])
    max_blank[10] = max(max_blank[10:12])
    max_blank[12] = max(max_blank[12:14])
    max_blank.pop(13)
    max_blank.pop(11)
    max_blank.pop(3)
    max_blank.pop(2)
    max_blank.pop(1)
    # print(max_blank)

    # print(missed_know)
    missed_know[0] = min(missed_know[0:4])
    missed_know[10] = min(missed_know[10:12])
    missed_know[12] = min(missed_know[12:14])
    missed_know.pop(13)
    missed_know.pop(11)
    missed_know.pop(3)
    missed_know.pop(2)
    missed_know.pop(1)
    # print(missed_know)
    #### ここまで ####

    end_time = df_list['time_stamp'].tail(1).values
    start_time = df_list['time_stamp'].head(1).values
    all_time = end_time - start_time

    draw_rate = np.array(df_list['time'].tail(1).values.tolist()).reshape(-1).tolist()
    draw_rate = draw_rate[1] / draw_rate[0]

    draw_num = [0, 0]
    l_draw_num = df_list['time'].T.values.tolist()

    renzoku = 0
    for i, draw_list in enumerate(l_draw_num, 0):
        #print("\n"+ str(i) + ": ")
        for k, elem in enumerate(draw_list, 0):
            draw_list[k] = draw_list[k] / 100
        for j, elem in enumerate(draw_list, 0):
            if(j == len(draw_list)):
                #print(Color.BLUE + "END" + Color.END, end="")
                break
            if(elem == draw_list[j-1]):
                renzoku = 1
                #print(Color.END + "%4d"%elem + Color.END, end=",")
            else:
                if(renzoku == 1):
                    draw_num[i] = draw_num[i] + 1
                    renzoku = 0
                    #print(Color.RED + "%4d"%elem + Color.END, end=",")
                 #else:
                 #   print(Color.GREEN + "%4d" % elem + Color.END, end=",")

    touch_interval = sum(touched) / (all_time/60)
    splited_touch_interval = list(np.array_split(touched, 4))
    for i, elem in enumerate(splited_touch_interval, 0):
        splited_touch_interval[i] = sum(elem) / (all_time/4/60)
        splited_touch_interval[i] = (splited_touch_interval[i][0])
    interval_var = [np.var(splited_touch_interval).tolist()]

    draw_rate = [draw_rate]
    draw_num = [draw_num[0]]

    all_time = all_time.tolist()
    touch_interval = touch_interval.tolist()

    var_touched = [np.var(touched).tolist()]
    not_zero_hovered = [elem for elem in hovered if i != 0]
    var_hovered = [np.var(not_zero_hovered).tolist()]

    # print(first)
    # print(touched)
    # print(hovered)
    # print(all_time)
    # print(draw_rate)
    # print(draw_num)
    # print(touch_interval)
    # print(splited_touch_interval)
    # print(var_touched)
    # print(var_hovered)

    sim_list = score_sim(df_list["code"], answer, filename)

    # print(sim_list)
    # print("最後：" + str(sim_list[len(sim_list)-1]))
    correct = [sim_list[len(sim_list)-1]]

    mx = 0
    downed = 0
    count = 0
    for i, val in enumerate(sim_list):
        if mx-0.005 > val:
            downed += 1
        else:
            mx = val
            downed = 0
        if downed > 1:
            # plt.scatter(i, sim_list[i])
            mx = val
            count += 1
            downed = 0
    missed = [count]
    # print(missed)
    #
    # plt.plot(sim_list, color ="orange", linewidth= 2, linestyle = "dashed")
    # plt.xlim(-5, 550)
    # plt.ylim(-0.1, 1.1)
    # plt.title(filename)
    # plt.savefig('sintyoku/'+filename+'.png')
    # plt.close()

    # 標準化
    first = preprocessing.scale(first).tolist()
    touched = preprocessing.scale(touched).tolist()
    hovered = preprocessing.scale(hovered).tolist()
    splited_touch_interval = preprocessing.scale(splited_touch_interval).tolist()

    # first.extend(touched.extend(hovered.extend(touched)))
    # print(first)
    EV_list = first + touched + hovered + missed_know + max_blank + correct + missed + all_time + draw_rate + draw_num + touch_interval + splited_touch_interval + interval_var + var_touched + var_hovered
    # print("説明変数：\n", len(EV_list), str(EV_list))
    label = []
    for i in range(20):
        label.append("first" + str(i+1))
    for i in range(20):
        label.append("touch" + str(i+1))
    for i in range(20):
        label.append("hover" + str(i+1))
    for i in range(12):
        label.append("word_down" + str(i+1))
    for i in range(12):
        label.append("word_correct" + str(i + 1))
    label.append("correctness")
    label.append("missed")
    label.append("all_time")
    label.append("draw_rate")
    label.append("draw_num")
    label.append("touch_interval")
    label.append("interval_1")
    label.append("interval_2")
    label.append("interval_3")
    label.append("interval_4")
    label.append("interval_var")
    label.append("var_touched")
    label.append("var_hovered")
    # print(len(label))

    return EV_list

def standardization(df):

    for i in range(len(df.T)-60-2):
        df.loc[:, len(df.T)-1-i-2] = preprocessing.scale(df.loc[:, len(df.T)-1-i-2]).tolist()

    return df
