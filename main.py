import pandas as pd
import myPlot
import myCreateEV
import myCreateRV
import pprint
import os
import myLogistic
import japanize_matplotlib

# 説明変数一覧
def show_exp(raw_learn_df, learn_df):
    print("______________________________________________________________________________________________________________________________________")
    label = []
    for i in range(20):
        label.append("first" + str(i+1))
    for i in range(20):
        label.append("touch" + str(i+1))
    for i in range(20):
        label.append("hover" + str(i+1))
    for i in range(12):
        label.append("空欄" + str(i+1) + "類似度低下回数")
    for i in range(12):
        label.append("空欄" + str(i + 1) + "類似度")
    label.append("模範解答類似度")
    label.append("類似度低下回数")
    label.append("合計取組時間")
    label.append("作図取組割合")
    label.append("作図切替回数")
    label.append("タッチ頻度")
    label.append("タッチ頻度~1/4時間")
    label.append("タッチ頻度~2/4時間")
    label.append("タッチ頻度~3/4時間")
    label.append("タッチ頻度~4/4時間")
    label.append("タッチ頻度分散")
    label.append("タッチ回数分散")
    label.append("ホバー秒数分散")
    label.append("知識力")
    label.append("応用力")

    raw_learn_df.columns = label
    raw_learn_df.to_csv("variables2.csv")
    learn_df.columns = label
    learn_df.to_csv("variable2_standardization.csv")

    print(raw_learn_df)



directory = '.\\data\\'


column_name = ('user','time_stamp','json','tab1_time','tab2_time','block_touch_num_0','block_hover_time_0','block_touch_num_1','block_hover_time_1','block_touch_num_2','block_hover_time_2','block_touch_num_3','block_hover_time_3','block_touch_num_4','block_hover_time_4','block_touch_num_5','block_hover_time_5','block_touch_num_6','block_hover_time_6','block_touch_num_7','block_hover_time_7','block_touch_num_8','block_hover_time_8','block_touch_num_9','block_hover_time_9','block_touch_num_10','block_hover_time_10','block_touch_num_11','block_hover_time_11','block_touch_num_12','block_hover_time_12','block_touch_num_13','block_hover_time_13','block_touch_num_14','block_hover_time_14','block_touch_num_15','block_hover_time_15','block_touch_num_16','block_hover_time_16','block_touch_num_17','block_hover_time_17','block_touch_num_18','block_hover_time_18','block_touch_num_19','block_hover_time_19','block_touch_num_20','block_hover_time_20','block_touch_num_21','block_hover_time_21')

label = (
    'while(input \空欄)',
    'run(' ', \空欄);',
    'int main()',
    'while(i \空欄)',
    'int input;',
    'int i;',
    'int input[][];',
    'printf("正の整数: ");',
    'run("*", \空欄);',
    'printf("\空欄", input);',
    'int input[];',
    'int i;',
    'for(i = \空欄; \空欄; i++)',
    'printf("\空欄", c);',
    '\空欄 run(\空欄 c, \空欄 length)',
    'scanf("\空欄", \空欄);',
    'for(input = \空欄; \空欄; input++)',
    'for(i = \空欄; \空欄; i++)',
    'printf("\空欄");',
    'printf("\空欄: ", i);'
)



list_filename = os.listdir(path=directory)

learn_df = pd.DataFrame(data=None, index=None, columns=None, dtype=None, copy=False)

for element in list_filename:

    # csvロード
    element = element.replace(".csv", "")
    raw_df = pd.read_csv(directory + element + ".csv", names=column_name, encoding="Shift-JIS")
    # データフレームを種類ごとに分ける
    df_user = raw_df.user
    df_time_stamp = raw_df.time_stamp
    df_code = raw_df.json
    df_time = raw_df.iloc[:, 3:5]
    df_touch = raw_df.iloc[:, 5:-4:2]
    df_hover = raw_df.iloc[:, 6:-4:2]
    df_list = [df_user, df_time_stamp, df_code, df_time, df_touch, df_hover]
    df_dict = {'user': df_user, 'time_stamp': df_time_stamp, 'code': df_code, 'time': df_time, 'touch': df_touch, 'hover' :df_hover}

    #### --------グラフのプロット-------- ####
    # myPlot.myPlot(df_list, element)

    # 説明変数・応答変数をロード
    myEV_list = myCreateEV.myCreateEV(df_dict, element)
    myRV_df = myCreateRV.myCreateRV(df_dict, element)

    # 説明変数リストに応答変数をガッチャンコ
    myEV_list.append(myRV_df['知識力'].tolist())
    myEV_list.append(myRV_df['論理力'].tolist())
    learn_df[element] = myEV_list


#説明変数＋応答変数のセット
learn_df = learn_df.T
raw_learn_df = learn_df.copy()
# print(learn_df)

#説明変数を標準化
learn_df = myCreateEV.standardization(learn_df)
# print(learn_df)

#分析
myLogistic.myLogistic(learn_df)

show_exp(raw_learn_df, learn_df)

