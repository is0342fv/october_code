import difflib
import Levenshtein
import re
import pprint
import pandas as pd
import copy

def scoreBlank(question, df_code, answers, filename):
    bad_words = ["intinput;", "inti;", "int input;", "int i;"]
    question = question.replace("_", "").replace(" ", "")
    for i, val in enumerate(answers):
        answers[i] = val.replace("_", "").replace(" ", "")

    question = numbering(question)
    sep_question = question.split("\n")

    delete = []
    for i, val in enumerate(sep_question):
        for bad_word in bad_words:
            if (bad_word in val):
                delete.append(i)
                # sep_question.pop(i)
    delete.reverse()
    for j, val in enumerate(delete):
        sep_question.pop(val)

    # print(sep_question)

    model_answer_df = pd.DataFrame(index = sep_question, columns = [])
    model_answer_df = model_answer_df.reset_index()

    #### 正解セットの作成 ####

    temp_sep_question = copy.copy(sep_question)
    for i, an_answer in enumerate(answers):
        sep_question = copy.copy(temp_sep_question)
        # print("第" + str(i+1) + "番目の答え")
        sep_an_answer = an_answer.split("\n")
        new_index = []
        new_column = []
        delete = []
        for i, answer_line in enumerate(sep_an_answer):
            for bad_word in bad_words:
                if (bad_word in answer_line):
                    delete.append(i)
                    # sep_question.pop(i)
        delete.reverse()
        for j, val in enumerate(delete):
            sep_an_answer.pop(val)
        # print(sep_an_answer)
        for j, answer_line in enumerate(sep_an_answer):
            max = 0
            for k, question_line in enumerate(sep_question):
                num = Levenshtein.ratio(sep_an_answer[j], sep_question[k])
                if max <= num:
                    max = num
                    max_k = k
            # print("----")
            sep_question[max_k] = ""
            # print(" " + sep_question[max_k])
            # print(" " + temp_sep_question[max_k])
            # print(" " + sep_an_answer[j])
            new_index.append(temp_sep_question[max_k])
            new_column.append(sep_an_answer[j])
        # print(new_index, new_column)
        another_model = pd.DataFrame(new_column, index = new_index, columns = ["Answer"+str(i+1)])
        # print(another_model.index)
        another_model = another_model.reset_index()
        model_answer_df = pd.merge(model_answer_df, another_model, how="outer")
        # model_answer_df.to_csv("sample"+str(i)+".csv")
    # print(model_answer_df)
    # model_answer_df.to_csv("sample.csv", encoding="SHIFT-JIS")

    new_index.clear()
    new_column.clear()
    model_answer_df = model_answer_df.drop("index", axis=1)

    # print(df_code)
    score = pd.DataFrame()
    score = score.reset_index()
    for i, a_code in enumerate(df_code):
        if a_code == a_code: # Null 判定
            a_code = a_code.replace("_", "")
            sep_a_code = a_code.split("\n")
            delete = []
            for k, answer_line in enumerate(sep_a_code):
                sep_a_code[k] = sep_a_code[k].replace("\r", "").replace(" ", "")
                for bad_word in bad_words:
                    if (bad_word in answer_line):
                        delete.append(k)
                        # sep_question.pop(i)
            delete.reverse()
            for l, val in enumerate(delete):
                sep_a_code.pop(val)

            a_code2 = ""
            for l, val in enumerate(sep_a_code):
                a_code2 = a_code2 + val + "\n"

            # print(a_code2)

            # print(sep_a_code)
            # print(model_an/swer_df['index'].tolist())

            # print(i, "番目コード＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝")
            new_index = []
            new_column = []
            # print(model_answer_df)
            for index_num, model_index in enumerate(model_answer_df.iterrows()):
                word_max = 0
                for k, a_model_row in enumerate(model_index[1]):
                    max = 0
                    # print(a_model_row)
                    if a_model_row != a_model_row:
                        continue
                    for line_num, code_line in enumerate(sep_a_code):
                        num = Levenshtein.ratio(a_model_row, code_line)
                        # print(a_model_row, "\t\t\t", code_line, "\t\t\t", num)
                        if max <= num:
                            max = num
                            max_line_num = line_num
                            good = a_model_row
                            good2 = code_line
                    # sep_a_code[line_num] = ""
                    # print("----")
                    # print(good)
                    # print(good2)
                    # print(max)
                    if word_max <= max:
                        word_max = max
                        best = a_model_row # もっとも近かったモデルのROW
                        best2 = good2 # コード
                # print(best + "\t\t\t\t" + best2 + str(word_max))
                # print(word_max)
                new_index.append(best)
                new_column.append(word_max)
                # print(best, "\t", best2, "\t", word_max)
            # print(new_index, "\n", new_column)
            new_df = pd.DataFrame(new_column, index = new_index, columns = [str(i+1)])
            new_df = new_df.reset_index()
            score = pd.merge(score, new_df, how="outer")
            score = score.drop_duplicates(subset="index")
        else:
            score[str(i+1)] = score.apply(lambda _: '', axis=1)
    score = score.drop_duplicates(subset="index")
    score.index = score["index"]
    score = score.drop("index", axis=1)
    score = score.fillna({"1": 0})
    score = score.T.fillna(method="ffill").T
    score = score.sort_index()
    # pprint.pprint(score)
    # score.to_csv(filename + "_knowledge.csv", encoding="SHIFT-JIS")

    return score

def numbering(string):

    count = 0
    blanks = []
    while True:
        before = string
        string = string.replace("\\空欄", "\\穴抜"+str(count), 1)
        count += 1
        if(string == before):
            break

    return string








